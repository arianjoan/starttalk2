import { FormGroup } from '@angular/forms';

export class CustomValidator {
    static checkPasswords(fg : FormGroup){
        let pass = fg.controls.password.value;
        let secondPassword = fg.controls.passwordRepeated.value;

        return pass === secondPassword ? null : { passwordsNotEquals : true}
    }
}
