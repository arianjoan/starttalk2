import { Injectable } from '@angular/core';
import { firebase } from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationFirebaseService {

  public db;

  constructor(private router: Router) {

    firebase.initializeApp({
      apiKey: "AIzaSyBxyIiAtrCq1k551w2liBIEJBj5uGwphLc",
      authDomain: "starttalk-d9acd.firebaseapp.com",
      databaseURL: "https://starttalk-d9acd.firebaseio.com",
      projectId: "starttalk-d9acd",
      storageBucket: "starttalk-d9acd.appspot.com",
      messagingSenderId: "117527871059",
      appId: "1:117527871059:web:312c88bcc22dbbc8f650a6",
      measurementId: "G-JEJRLJNWHG"
    });
    this.db = firebase.firestore();
  }

  addUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password).catch((error) => console.log('Error al crear usuario' + error));
  }

  login(email: string, password: string): Promise<any> {

    firebase.auth().signOut().then(() => console.log('sesion cerrada'));

    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {

          firebase.auth().currentUser.getIdToken()
            .then((jwt) => {
              localStorage.setItem('firebaseToken', jwt);
              localStorage.setItem('email', firebase.auth().currentUser.email);
              /* this.router.navigate(['landing']); */
              resolve(jwt);
            });

        })
        .catch((error) => {
          console.log(error)
          reject(error);
        });
    })

    /*   firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {
  
          firebase.auth().currentUser.getIdToken()
            .then((jwt) => {
              localStorage.setItem('firebaseToken', jwt);
              localStorage.setItem('email', firebase.auth().currentUser.email);
              this.router.navigate(['landing']);
            });
  
        })
        .catch((error) => {
          console.log(error)
        }); */
  }


}
