import { Component, OnInit } from '@angular/core';
import { AuthenticationFirebaseService } from 'src/app/services/authentication-firebase.service';
import { User } from 'src/app/models/user';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private user: User = new User();
  private formUserGroup: FormGroup;
  private error : boolean = false;

  constructor(private firebase: AuthenticationFirebaseService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.formUserGroup = this.fb.group({
      email: [this.user.email],
      password: [this.user.password]
    });
  }

  public logIn() {
    this.user = this.formUserGroup.value;
    try {

      this.firebase.login(this.user.email, this.user.password)
      .then((response) => {
        this.router.navigate(['landing']);
      })
      .catch((error) => {
        this.error = true;
        console.log('Credenciales Invalidas');
      })
      
    } catch (error) {

      console.log(error);

    }
  }
}
