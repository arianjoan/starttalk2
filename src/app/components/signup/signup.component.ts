import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthenticationFirebaseService } from 'src/app/services/authentication-firebase.service';
import { CustomValidator } from 'src/app/models/custom-validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  private user : User = new User();
  private formUserGroup: FormGroup;

  constructor(private firebase : AuthenticationFirebaseService, private fb : FormBuilder, private router : Router) { }

  ngOnInit() {
    this.formUserGroup = this.fb.group({
      email : [this.user.email],
      password : [this.user.password],
      passwordRepeated : [this.user.password]
    },{validator : CustomValidator.checkPasswords});
  }
  
  public signUp(){
    this.user = this.formUserGroup.value;
    this.firebase.addUser(this.user.email,this.user.password);
    this.router.navigate(['/login']);
  }
}
