/* import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    localStorage.removeItem('name');
  }

}
 */

 import {Component} from '@angular/core';
import { ChannelService } from 'src/app/services/channel.service';

 @Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent {

  showFiller = false;

  constructor(private serviceChannel : ChannelService) { }

  ngOnInit(): void {
    localStorage.removeItem('name');
  }

  name() {
    return this.serviceChannel.getCurrentChannel().name;
  }
  
} 